console.log('hello');

function printMessage(message){
	console.log('hello');
}
printMessage('message');
printMessage('message');
printMessage('message');
printMessage('message');
printMessage('message');


let count = 5;

// while(count !== 0) {
// 	printMessage();
// 	count--;
// }

//Do while loop

// do {
// 	console.log(count);
// 	count--;
// } while (count === 0);

// while (count === 0){
// 	console.log(count);
// 	count--;
// }

let counterMini = 20;

while (counterMini > 0){
	console.log(counterMini);
	counterMini--;
}
//for loop
//more fexible version of while and do while loop
//it consists of 3 parts
//1.is the declaration/initialization of the counter
//2.COndition that will be evaluated to determine
//if the loop will continue
//3. the iteration or the incrementation/decrementation
//needed to continue and arrive at terminating/end condition

for(let count = 0; count <= 20; count++){
	console.log(count);
}
//first loop = count = before we end the loop -> count =1
// sencond loop = count = 1 before we end loop -> count =2
//until we reach 20
//2nd to the last loop = count =19 = before we count the loop -> 20
// last loop = count = 20 = before we end the last loop -> 21
//will we run another loop> no because count no longer meets oour condition

for (let x = 0; x < 10; x++){
	let sum = 1 + x;
	console.log("the sum of " + 1 + "+" + x + "= " + sum);
}
for (let counter = 0; counter <= 20; counter++){
	if(counter % 2 === 0){
		continue;
	//when use continue keyword the code blocks the following is disregarded
	}

		console.log(counter)
		/*
		break- allows us to end the execution
		*/
		if(counter > 10){
		break;
		//when counter === 11
	}	
}
for (let number = 0; number <= 100; number++){
	if(number % 5 === 0){
		// continue;
	
	console.log(number);
	}
}





























